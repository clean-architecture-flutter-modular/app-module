import 'package:flutter/material.dart';

import 'app_view.dart';

class AppState extends StatefulWidget {
  const AppState({super.key});

  @override
  State<AppState> createState() => AppView();
}
