import 'package:flutter/material.dart';
import 'package:presentation/presentation.dart';

import 'app_state.dart';

final class AppView extends State<AppState> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Arquitectura limpia con Flutter',
          initialRoute: Routes.splash,
          routes: appRoutes),
    );
  }
}
