// ignore_for_file: depend_on_referenced_packages
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:data/data.dart';
import 'package:domain/domain.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:presentation/presentation.dart';
import 'package:provider/provider.dart';
import 'package:url_strategy/url_strategy.dart';

import 'src/app/app_state.dart';

void main() async {
  setPathUrlStrategy();
  WidgetsFlutterBinding.ensureInitialized();
  LocaleSettings.useDeviceLocale();
  Intl.defaultLocale = LocaleSettings.currentLocale.languageTag;

  final languageService = LanguageService(
    LocaleSettings.currentLocale.languageCode,
  );
  final sessionService = SessionService(
    const FlutterSecureStorage(),
  );
  final connectivity = ConnectivityImpl(
    Connectivity(),
    InternetChecker(),
  );

  final http = Http(
    client: Client(),
    baseUrl: const String.fromEnvironment('BASE_URL'),
    apiKey: const String.fromEnvironment('TMDB_KEY'),
  );

  final accountAPI = AccountAPI(
    http,
    sessionService,
  );

  await connectivity.initialize();

  runApp(
    MultiProvider(
      providers: [
        Provider<AccountRepository>(
          create: (_) => AccountRepositoryImpl(
            accountAPI,
            sessionService,
          ),
        ),
        Provider<ConnectivityRepository>(
          create: (_) => connectivity,
        ),
        Provider<AuthenticationRepository>(
          create: (_) => AuthenticationRepositoryImpl(
            AuthenticationApi(http),
            accountAPI,
            sessionService,
          ),
        ),
        Provider<LanguageRepository>(
          create: (_) => LanguageRepositoryImpl(
            languageService,
          ),
        ),
        ChangeNotifierProvider<SessionController>(
          create: (context) => SessionController(
            authenticationRepository: context.read(),
          ),
        ),
        Provider<TrendingRepository>(
          create: (_) => TrendingRepositoryImpl(
            TrendingAPI(http, languageService),
          ),
        )
      ],
      child: TranslationProvider(
        child: const AppState(),
      ),
    ),
  );
}
